import { reactive, watchEffect } from "vue";


const store = reactive<any>({
    spinners: [] as any,
    selected: [] as any,
    apidata: {} as any
})

export const loadData = () => {
    store.spinners = [
        "https://images-na.ssl-images-amazon.com/images/I/711Hao5KXKL.png",
        "https://images-na.ssl-images-amazon.com/images/I/711Hao5KXKL.png",
        "https://images-na.ssl-images-amazon.com/images/I/711Hao5KXKL.png",
        "https://images-na.ssl-images-amazon.com/images/I/711Hao5KXKL.png",
        "https://images-na.ssl-images-amazon.com/images/I/711Hao5KXKL.png",
        "https://www.cdiscount.com/pdt2/2/0/2/1/1200x1200/gen3760308740202/rw/hand-spinner-metal-couleur-rainbow-arc-en-ciel-fr.jpg",
        "https://media.istockphoto.com/photos/fidget-spinner-with-rainbow-flag-theme-picture-id802517310",
        "https://content.pearl.fr/media/cache/default/article_ultralarge_high_nocrop/shared/images/articles/K/KT7/hand-spinner-3-branches-ref_KT7963_1.jpg",
    ]
}

export const addSpinners = (data: any) => {
    store.spinners.push(data);
}
export const addSelected = (data: any) => {
    store.selected.push(data);
}
export const removeSelected = (dataToRemove: any) => {
    const index = store.selected.indexOf(dataToRemove);
    if (index > -1) {
        store.selected.splice(index, 1);
    }
}


type Callback = (store: any) => void;
const subscribers: Callback[] = [];
export const subscribe = (callback: Callback) => {
  callback(store);
  subscribers.push(callback);
};

watchEffect(() => {
  const payload = {
    ...store,
  };
  subscribers.forEach((fn) => fn(payload));
});


export default store;