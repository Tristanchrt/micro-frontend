# Micro-fronted app Vuejs

## Install project 

```
cd home-spinner && yarn
cd list-spinner && yarn
cd shop-spinner && yarn
cd store && yarn
```
## Run project 

```
cd home-spinner && yarn start
cd list-spinner && yarn start
cd shop-spinner && yarn start
cd store && yarn start
```